#pragma once

#include <cmath>

template<typename numeric>
inline numeric abs(const numeric& n) {
    if (n < 0) return -n;
    return n;
}

template<typename numeric>
inline numeric squareDistance(numeric x, numeric y, numeric z, numeric px, numeric py, numeric pz) {
    numeric a = x - px, b = y - py, c = z - pz;
    return a*a + b*b + c*c;
}

template<typename numeric>
inline numeric getDistance(numeric x, numeric y, numeric z, numeric px, numeric py, numeric pz) {
    return static_cast<numeric>(sqrt(squareDistance<double>(x, y, z, px, py, pz)));
}

inline int getGCD(int count, int numbers[]) {
    int multiplier = 1;
    int gcd;

    for (int i = 0; i < count; ++i) numbers[i] = abs(numbers[i]);

    auto sequence_simplified = [count, numbers](){
        int total = 0;
        for (int i = 0; i < count; ++i) total += numbers[i];
        if (total == 0) return 1;

        bool all_equal = true;
        for (int i = 0; i < count; ++i) {
            auto num = numbers[i];
            if (num == total) return total; // one number contain entire sum, other numbers are zeroes.
            if (num == 1) return 1; // sequence cannot be simplified further.
            if (all_equal && i > 0 && num != numbers[i - 1]) all_equal = false;
        }
        if (all_equal) return numbers[0];
        return -1;
    };

    while (true) {
        gcd = sequence_simplified();
        if (gcd >= 0) break;

        // count even numbers
        int even_count = 0;
        for (int i = 0; i < count; ++i) {
            auto num = numbers[i];
            if (num > 0 && (num & 1) == 0) {
                ++even_count;
                numbers[i] = num >> 1;
            }
        }

        if (even_count == count) multiplier <<= 1;
        if (even_count == 0) {
            int min_num = numbers[0], max_num = numbers[0], max_pos = 0;
            for (int i = 1; i < count; ++i) {
                auto val = numbers[i];
                if (val > max_num) {
                    max_num = val;
                    max_pos = i;
                } else if (val < min_num) {
                    min_num = val;
                }
            }
            numbers[max_pos] = (max_num - min_num) >> 1;
        }
    }

    return gcd * multiplier;
}

