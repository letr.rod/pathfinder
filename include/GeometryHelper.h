#pragma once

#include <utility>
#include "VectorMath.h"

template<typename numeric>
struct BBox {
    numeric minX;
    numeric maxX;
    numeric minY;
    numeric maxY;
    numeric minZ;
    numeric maxZ;

    BBox() = default;

    BBox(point3d<numeric> from, point3d<numeric> to) {
        set(from.x, from.y, from.z);
        adjust(to.x, to.y, to.z);
    };

    void set(numeric x, numeric y, numeric z) {
        minX = x;
        maxX = x;
        minY = y;
        maxY = y;
        minZ = z;
        maxZ = z;
    };

    void clear() {
        set(0, 0, 0);
    };

    void adjust(numeric x, numeric y, numeric z) {
        minX = std::min(minX, x);
        maxX = std::max(maxX, x);
        minY = std::min(minY, y);
        maxY = std::max(maxY, y);
        minZ = std::min(minZ, z);
        maxZ = std::max(maxZ, z);
    };

    void make_relative(numeric value) {
        minX = static_cast<numeric>(minX / value);
        maxX = static_cast<numeric>(maxX / value);
        minY = static_cast<numeric>(minY / value);
        maxY = static_cast<numeric>(maxY / value);
        minZ = static_cast<numeric>(minZ / value);
        maxZ = static_cast<numeric>(maxZ / value);
    };

    bool intersect(const BBox<numeric>& other) const {
        return !(other.minX > maxX || other.maxX < minX
            || other.minY > maxY || other.maxY < minY
            || other.minZ > maxZ || other.maxZ < minZ);
    }
};

template<typename numeric>
struct PlaneDefinition {
    numeric A;
    numeric B;
    numeric C;
    numeric D;
};

template<typename numeric>
void stepFromPoint(point3d<numeric> &acceptor, const point3d<numeric> &from, numeric length, const vec3d<double> &ortho_cosine){
    acceptor.x = static_cast<numeric>(ortho_cosine.x * length + from.x);
    acceptor.y = static_cast<numeric>(ortho_cosine.y * length + from.y);
    acceptor.z = static_cast<numeric>(ortho_cosine.z * length + from.z);
};

template<typename numeric>
bool pointsOnLine(const point3d<numeric> &p1, const point3d<numeric> &p2, const point3d<numeric> &p3) {
    auto vec1 = vec3d<numeric>(p1, p2);
    vec1.simplify();
    auto vec2 = vec3d<numeric>(p1, p3);
    vec2.simplify();
    auto mul = vec3d<numeric>::template vector_mul<int>(vec1, vec2);

    return mul.zero();
}

template<typename precise_numeric, typename numeric>
bool pointInBound(const point3d<precise_numeric> &point, const point3d<numeric> &upper, const point3d<numeric> &lower) {
    return point.x >= std::min(lower.x, upper.x) && point.x <= std::max(lower.x, upper.x)
        && point.y >= std::min(lower.y, upper.y) && point.y <= std::max(lower.y, upper.y)
        && point.z >= std::min(lower.z, upper.z) && point.z <= std::max(lower.z, upper.z);
}

/*
 * takes point and polygon, returns true, if point belongs to polygon.
 * Note: point must be beforehand projected to plane of polygon for correct result.
 */
template<typename precise_numeric, typename numeric>
bool pointInProjection(const point3d<precise_numeric> &point, int vertex_count, const point3d<numeric> *polygon) {
    const double double_pi = 6.28;//3185;
    double total_angle = 0;

    // changes total angle, returns true, if point is on the edge being checked
    auto process_edge = [&total_angle, point](const point3d<numeric> &v1, const point3d<numeric> &v2) {
        if (v1 == v2) return false;

        auto vec1 = vec3d<precise_numeric>(point, v1);
        auto vec2 = vec3d<precise_numeric>(point, v2);

        auto mul = vec3d<precise_numeric>::template vector_mul<double>(vec1, vec2);
        if (mul.zero()) {
            if (pointInBound(point, v1, v2)) return true;
        } else if (mul.z > 0) {
            total_angle += vec1.angle(vec2);
        } else {
            total_angle -= vec1.angle(vec2);
        }
        return false;
    };

    if (process_edge(polygon[vertex_count - 1], polygon[0])) return true;
    for (int i = 1; i < vertex_count; ++i) {
        if (process_edge(polygon[i - 1], polygon[i])) return true;
    }
    return abs(total_angle) >= double_pi;
}

template<typename pd_size, typename source>
PlaneDefinition<pd_size> getPlaneDefinition(const point3d<source> &v1, const point3d<source> &v2, const point3d<source> &v3) {
    auto vec1 = vec3d<source>(v1, v2);
    vec1.simplify();
    auto vec2 = vec3d<source>(v1, v3);
    vec2.simplify();
    auto mul = vec3d<source>::template vector_mul<pd_size>(vec1, vec2);

    PlaneDefinition<pd_size> result{mul.x, mul.y, mul.z, 0};
    result.D -= v1.x * mul.x + v1.y * mul.y + v1.z * mul.z;
    return result;
}

template<typename pd_size, typename source, typename result>
bool planeLineIntersection(const point3d<source> &line_point, const vec3d<source> &line_vec, const PlaneDefinition<pd_size> &pd, point3d<result> &acceptor) {
    auto divider = pd.A * line_vec.x + pd.B * line_vec.y + pd.C * line_vec.z;
    if (divider == 0) return false;

    auto dividend = -pd.A * line_point.x - pd.B * line_point.y - pd.C * line_point.z - pd.D;
    double t = static_cast<double>(dividend) / divider;

    double x = static_cast<double>(line_vec.x) * t + static_cast<double>(line_point.x);
    double y = static_cast<double>(line_vec.y) * t + static_cast<double>(line_point.y);
    double z = static_cast<double>(line_vec.z) * t + static_cast<double>(line_point.z);

    acceptor.x = static_cast<result>(x);
    acceptor.y = static_cast<result>(y);
    acceptor.z = static_cast<result>(z);
    return true;
}
