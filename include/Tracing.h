#pragma once

#include "GeometryHelper.h"
#include "DataModel.h"

struct FloorInfo {
    const Facet* floor;
    SpaceSize level;
    bool in_void;
};

bool traceLine(const std::vector<Facet> &facets, const point3d<SpaceSize> &from, const point3d<SpaceSize> &to);

bool traceLine(const std::vector<const std::vector<Facet>*> &groups, const point3d<SpaceSize> &from, const point3d<SpaceSize> &to);

void getFloorLevel(const std::vector<Facet> &facets, const point3d<SpaceSize> &position, FloorInfo& acceptor);

void getFloorLevel(const std::vector<const std::vector<Facet>*> &groups, const point3d<SpaceSize> &position, FloorInfo& acceptor);


