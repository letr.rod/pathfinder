#pragma once

#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

class Task {
public:
    enum Status {
        empty = 0,
        queued = 1,
        active = 2,
        completed = 3,
        error = 4,
        interrupted = 5
    };

    Status status = empty;

    virtual ~Task() = default;
    virtual void start() = 0;
    virtual unsigned int get_result(void *acceptor) = 0;
};

class TaskQueue {
public:
    int add_task(Task* task);
    Task::Status task_status(int task_id) const;
    unsigned int task_result(int task_id, void *acceptor);

    int get_max_threads() const;
    void set_max_threads(int value);

    int get_max_tasks() const;
    void set_max_tasks(int value);

    int size() const;

    void stop();

    TaskQueue();
    ~TaskQueue();
    TaskQueue(const TaskQueue& other) = delete;
    TaskQueue& operator=(const TaskQueue& other) = delete;

private:
    bool interrupt;
    int max_tasks;
    int max_threads;
    int threads_active;
    std::vector<Task*> tasks;
    std::queue<int> queue;
    std::mutex tasks_access;
    std::mutex work_in_progress;
    std::condition_variable wip;

    Task* pop_next_task();
    void handler();
};