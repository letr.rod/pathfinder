#pragma once

#ifdef __PATHFINDER_DEBUG__

#include <iostream>
#include <fstream>

#define DEBUG_LOG(MSG) {        \
    std::ofstream debug_log("pathfinder_debug.log", std::ios::app); \
    debug_log << MSG;           \
    debug_log.close();          \
}

#define DEBUG_MSG(MSG) {        \
    std::cout << MSG;           \
}

#define DEBUG_LOG_COND(BOOL_V, MSG1, MSG2) { \
    if (BOOL_V) {                        \
        DEBUG_LOG(MSG1);                 \
    } else {                             \
        DEBUG_LOG(MSG2);                 \
    }                                    \
}

#define DEBUG_MSG_COND(BOOL_V, MSG1, MSG2) { \
    if (BOOL_V) {                        \
        DEBUG_MSG(MSG1);                 \
    } else {                             \
        DEBUG_MSG(MSG2);                 \
    }                                    \
}

#else
#define DEBUG_LOG(MSG) {};
#define DEBUG_MSG(MSG) {};
#define DEBUG_LOG_COND(BOOL, MSG1, MSG2) {};
#define DEBUG_MSG_COND(BOOL, MSG1, MSG2) {};

#endif