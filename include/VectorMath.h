#pragma once

#include "MathHelper.h"

template<typename numeric>
struct point3d{
    numeric x;
    numeric y;
    numeric z;

    bool equal(const point3d &other, numeric precision = 0) const {
        return abs(x - other.x) <= precision && abs(y - other.y) <= precision && abs(z - other.z) <= precision;
    };

    bool operator==(const point3d &other) const {
        return equal(other);
    };

    point3d() = default;
    point3d(int _x, int _y, int _z) {
        x = static_cast<numeric>(_x);
        y = static_cast<numeric>(_y);
        z = static_cast<numeric>(_z);
    };
    point3d(short _x, short _y, short _z) {
        x = static_cast<numeric>(_x);
        y = static_cast<numeric>(_y);
        z = static_cast<numeric>(_z);
    };
};

template<typename numeric>
numeric getDistance(point3d<numeric> from, point3d<numeric> to) {
    return getDistance(from.x, from.y, from.z, to.x, to.y, to.z);
}

template<typename numeric>
struct vec3d {
    numeric x;
    numeric y;
    numeric z;

    template<typename result>
    vec3d<result> convert() {
        result _x, _y, _z;
        _x = static_cast<result>(x);
        _y = static_cast<result>(y);
        _z = static_cast<result>(z);
        return {_x, _y, _z};
    }

    [[nodiscard]] vec3d<double> ortho_cosine() const {
        double _x = cosine({1, 0, 0});
        double _y = cosine({0, 1, 0});
        double _z = cosine({0, 0, 1});
        return {_x, _y, _z};
    };

    template<typename result>
    static vec3d<result> vector_mul(const vec3d<numeric>& v1, const vec3d<numeric>& v2) {
        result x, y, z, x1, y1, z1, x2, y2, z2;
        x1 = static_cast<result>(v1.x);
        y1 = static_cast<result>(v1.y);
        z1 = static_cast<result>(v1.z);
        x2 = static_cast<result>(v2.x);
        y2 = static_cast<result>(v2.y);
        z2 = static_cast<result>(v2.z);
        x = y1 * z2 - y2 * z1;
        y = z1 * x2 - z2 * x1;
        z = x1 * y2 - x2 * y1;
        return {x, y, z};
    };

    //// constructors

    vec3d() = default;

    vec3d(numeric _x, numeric _y, numeric _z): x(_x), y(_y), z(_z) {};

    template<typename second>
    vec3d(const point3d<numeric>& from, const point3d<second>& to) {
        x = to.x - from.x;
        y = to.y - from.y;
        z = to.z - from.z;
    };

    //// operations

    void simplify() {
        if (std::is_floating_point<numeric>()) {
            numeric sum = x + y + z;
            x = x / sum;
            y = y / sum;
            z = z / sum;
        } else {
            int list[3];
            list[0] = static_cast<int>(x);
            list[1] = static_cast<int>(y);
            list[2] = static_cast<int>(z);
            auto gcd = getGCD(3, list);
            if (gcd == 1) return;
            x /= gcd;
            y /= gcd;
            z /= gcd;
        }
    };

    [[nodiscard]] bool zero() const {
        return x == 0 && y == 0 && z == 0;
    };

    [[nodiscard]] double mod() const {
        return sqrt(x*x + y*y + z*z);
    };

    [[nodiscard]] double scalar_mul(const vec3d<numeric>& other) const {
        return static_cast<double>(x * other.x + y * other.y + z * other.z);
    };

    [[nodiscard]] double cosine(const vec3d<numeric>& other) const {
        return scalar_mul(other) / (mod() * other.mod());
    };

    [[nodiscard]] double angle(const vec3d<numeric>& other) const {
        return acos(cosine(other));
    };

};



