#include "MapData.h"

//// Object struct

Object::Object(bool _flying, int _radius, int _step_length, int _step_height) {
    flying = _flying;
    radius = static_cast<SpaceSize>(_radius);
    step_length = static_cast<SpaceSize>(_step_length);
    step_height = static_cast<SpaceSize>(_step_height);
}

//// MapData class

void MapData::sector_from_point(int &x, int &y, int &z) const {
    x /= cube_area_side;
    y /= cube_area_side;
    z /= cube_area_side;
}

void MapData::sector_from_point(SpaceSize &x, SpaceSize &y, SpaceSize &z) const {
    x = static_cast<SpaceSize>(x / cube_area_side);
    y = static_cast<SpaceSize>(y / cube_area_side);
    z = static_cast<SpaceSize>(z / cube_area_side);
}

const std::vector<Facet>* MapData::facet_group_by_sector(int x, int y, int z) const {
    auto x_group = facet_groups.find(x);
    if (x_group == facet_groups.end()) return nullptr;

    auto y_group = x_group->second.find(y);
    if (y_group == x_group->second.end()) return nullptr;

    auto sector = y_group->second.find(z);
    if (sector == y_group->second.end()) return nullptr;

    return &sector->second;
}

const std::vector<Facet>* MapData::facet_group_from_point(SpaceSize x, SpaceSize y, SpaceSize z) const {
    sector_from_point(x, y, z);
    return facet_group_by_sector(x, y, z);
}

std::vector<const std::vector<Facet>*> MapData::facet_groups_on_vertical(Vertex from, Vertex to, bool grounded) const {
    std::vector<const std::vector<Facet>*> result;

    int z_end = grounded ? bounding_box.minZ : to.z / cube_area_side;
    int x = from.x, y = from.y, z = from.z;
    sector_from_point(x, y, z);

    for (; z >= z_end; --z) {
        auto group = facet_group_by_sector(x, y, z);
        if (group != nullptr) result.push_back(group);
    }
    return result;
}

std::vector<const std::vector<Facet>*> MapData::facet_groups_on_axis(Vertex from, Vertex to, vec3d<SpaceSize> vec, bool grounded) const {
    std::vector<const std::vector<Facet>*> result;

    sector_from_point(from.x, from.y, from.z);
    sector_from_point(to.x, to.y, to.z);

    vec.x = vec.x > 0 ? 1 : 0;
    vec.y = vec.y > 0 ? 1 : 0;

    while (from.x <= to.x && from.y <= to.y) {
        auto group = facet_group_by_sector(from.x, from.y, from.z);
        if (group != nullptr) result.push_back(group);

        if (grounded) {
            for (SpaceSize z = from.z; z >= bounding_box.minZ; --z) {
                group = facet_group_by_sector(from.x, from.y, z);
                if (group != nullptr) result.push_back(group);
            }
        }

        if (vec.x > 0) ++from.x;
        else ++from.y;
    }

    return result;
}

std::vector<const std::vector<Facet>*> MapData::facet_groups_on_line(Vertex from, Vertex to, bool grounded) const {
    auto int_vec = vec3d(from, to);
    if (int_vec.x == 0 && int_vec.y == 0) {
        return facet_groups_on_vertical(from, to, grounded);
    } else if (int_vec.z == 0 && (int_vec.x == 0 || int_vec.y == 0)) {
        return facet_groups_on_axis(from, to, int_vec, grounded);
    }

    std::vector<const std::vector<Facet>*> result;
    std::set<const std::vector<Facet>*> unique;

    auto shift = static_cast<SpaceSize>(cube_area_side / 2);
    auto length = getDistance(from, to) + shift;
    auto line_vec = int_vec.convert<double>();
    auto or_cos = line_vec.ortho_cosine();
    vec3d<double> or_cos90 {-or_cos.y, or_cos.x, or_cos.z};

    auto process_point = [this, &unique](int x, int y, int z, bool grounded){
        sector_from_point(x, y, z);
        const std::vector<Facet> *group = facet_group_by_sector(x, y, z);
        if (group != nullptr) unique.insert(group);

        if (grounded) {
            for (z -= 1; z >= bounding_box.minZ; --z) {
                group = facet_group_by_sector(x, y, z);
                if (group != nullptr) unique.insert(group);
            }
        }
    };

    auto process_step = [shift, grounded, process_point, or_cos90](const Vertex& pos){
        Vertex side_pos;
        process_point(pos.x, pos.y, pos.z, grounded);

        stepFromPoint(side_pos, pos, static_cast<SpaceSize>(shift), or_cos90);
        process_point(side_pos.x, side_pos.y, side_pos.z, false);

        stepFromPoint(side_pos, pos, static_cast<SpaceSize>(-shift), or_cos90);
        process_point(side_pos.x, side_pos.y, side_pos.z, false);
    };

    process_step(from);
    Vertex pos;
    for (ExtSpaceSize i = cube_area_side; i <= length; i += cube_area_side) {
        stepFromPoint(pos, from, static_cast<SpaceSize>(i), or_cos);
        process_step(pos);
    }
    process_step(to);

    for (auto group : unique) result.push_back(group);

    return result;
}

void MapData::make_facet_groups() {
    facet_groups.clear();
    bounding_box.clear();

    auto assign_facet = [this](const Facet& facet, SpaceSize x, SpaceSize y, SpaceSize z){
        facet_groups[x / cube_area_side][y / cube_area_side][z / cube_area_side].push_back(facet);
    };

    SpaceSize tx, ty, tz;
    for (auto& facet : facets) {
        const auto& bbox = facet.bbox;
        for (ExtSpaceSize x = bbox.minX; x < bbox.maxX + cube_area_side; x += cube_area_side) {
            tx = static_cast<SpaceSize>(std::min(x, static_cast<ExtSpaceSize>(bbox.maxX)));
            for (ExtSpaceSize y = bbox.minY; y < bbox.maxY + cube_area_side; y += cube_area_side) {
                ty = static_cast<SpaceSize>(std::min(y, static_cast<ExtSpaceSize>(bbox.maxY)));
                for (ExtSpaceSize z = bbox.minZ; z < bbox.maxZ + cube_area_side; z += cube_area_side) {
                    tz = static_cast<SpaceSize>(std::min(z, static_cast<ExtSpaceSize>(bbox.maxZ)));
                    assign_facet(facet, tx, ty, tz);
                }
            }
        }
        bounding_box.adjust(bbox.minX, bbox.minY, bbox.minZ);
        bounding_box.adjust(bbox.maxX, bbox.maxY, bbox.maxZ);
    }

    bounding_box.make_relative(cube_area_side);
}

void MapData::import_obj(const std::string& obj_filepath) {
    std::ifstream file(obj_filepath);
    if (!file.is_open()) {
        std::string error_str = "Could not open file " + obj_filepath;
        throw std::runtime_error(error_str);
    }

    facets.clear();

    std::vector<Vertex> vertexes;
    auto add_vertex = [&vertexes](std::stringstream& coords) {
        std::string sx, sy, sz;
        coords >> sx >> sy >> sz;
        double x, y, z;

        // no try-catch construction - let test fall, if the file is not supported
        x = std::stod(sx);
        y = std::stod(sy);
        z = std::stod(sz);

        vertexes.emplace_back(static_cast<SpaceSize>(x), static_cast<SpaceSize>(y), static_cast<SpaceSize>(z));
    };

    auto add_facet = [this, &vertexes](std::stringstream& info){
        facets.emplace_back();
        Facet& facet = facets.back();

        facet.bits = &Facet::default_ground_bits;
        facet.polygon_type = &Facet::default_ground_type;

        std::string v_buf;
        while (info >> v_buf) {
            std::stringstream vertex_info(v_buf);
            std::string vertex_id;
            while (std::getline(vertex_info, vertex_id, '/')) {
                unsigned int id = std::stoi(vertex_id);
                if (id > vertexes.size()) throw std::runtime_error("Facet vertex id greater than vertexes count.");
                facet.vertexes.push_back(vertexes[id-1]);
            }
        }

        facet.form_bbox();
    };

    std::string line_buf;
    while (std::getline(file, line_buf)) {
        std::stringstream line(line_buf);
        std::string first_token;
        line >> first_token;
        if (first_token == "v") {
            add_vertex(line);
        } else if (first_token == "f") {
            add_facet(line);
        }
    }

    make_facet_groups();
}

void MapData::dump(const char *filepath) const {
    std::ofstream obj(filepath);
    if (!obj.is_open()) return;

    obj << "o Model" << std::endl;

    int current_vertex = 0;
    std::stringstream vertexes, polygons;
    for (auto& facet : facets) {
        if (facet.no_block()) continue;

        polygons << 'f';
        for (auto& vertex : facet.get_vertexes()) {
            vertexes << "v " << vertex.x << ' ' << vertex.y << ' ' << vertex.z << std::endl;
            ++current_vertex;
            polygons << ' ' << current_vertex;
        }
        polygons << std::endl;
    }
    obj << vertexes.str();
    obj << polygons.str();
    obj.close();
}



