#include "Tracing.h"

template<typename pd_size>
PlaneDefinition<pd_size> facetPlaneDefinition(const std::vector<Vertex> &vertexes) {
    unsigned int vertexes_count = vertexes.size();

    if (vertexes_count < 3) return {0,0,0,0};

    auto first = vertexes[0];
    auto second = vertexes[1];
    unsigned int next = 2;
    while (next < vertexes_count && first == second) {
        second = vertexes[next];
        ++next;
    }
    if (next >= vertexes_count) return {0,0,0,0};

    auto third = vertexes[next];
    while (next < vertexes_count && pointsOnLine(first, second, third)) {
        third = vertexes[next];
        ++next;
    }
    if (next > vertexes_count) return {0,0,0,0};

    return getPlaneDefinition<pd_size>(first, second, third);
}

bool traceLine(const std::vector<Facet> &facets, const point3d<SpaceSize> &from, const point3d<SpaceSize> &to) {
    std::vector<const std::vector<Facet>*> groups{&facets};
    return traceLine(groups, from, to);
}

bool traceLine(const std::vector<const std::vector<Facet>*> &groups, const point3d<SpaceSize> &from, const point3d<SpaceSize> &to) {
    BoundingBox line_bbox(from, to);

    auto line_vec = vec3d(from, to);
    line_vec.simplify();

    for (auto group : groups) {
        auto &facets = *group;
        for (auto &facet : facets) {
            if (facet.no_block()) continue;
            if (facet.dynamic()) const_cast<Facet&>(facet).form_bbox();
            if (!line_bbox.intersect(facet.bbox)) continue;

            point3d<double> intersection{};
            const auto& vertexes = facet.get_vertexes();
            auto facet_pd = facetPlaneDefinition<int>(vertexes);
            if (!planeLineIntersection(from, line_vec, facet_pd, intersection)) continue;
            if (!pointInProjection(intersection, vertexes.size(), vertexes.data())) continue;
            if (!pointInBound(intersection, from, to)) continue;

            return false;
        }
    }

    return true;
}

void getFloorLevel(const std::vector<Facet> &facets, const point3d<SpaceSize> &position, FloorInfo& acceptor) {
    std::vector<const std::vector<Facet>*> groups{&facets};
    getFloorLevel(groups, position, acceptor);
}

void getFloorLevel(const std::vector<const std::vector<Facet>*> &groups, const point3d<SpaceSize> &position, FloorInfo& acceptor) {
    struct candidate {
        const Facet* facet;
        double z_intersection;
        bool ceiling;

        candidate(const Facet* _f, double _z, bool _ceil): facet(_f), z_intersection(_z), ceiling(_ceil) {};
    };

    auto line_vec = vec3d<SpaceSize>{0, 0, -1};
    std::vector<candidate> candidates;

    for (auto &facets : groups) {
        for (auto &facet : *facets) {
            if (!facet.is_floor() && !facet.is_ceiling()) continue;
            if (facet.no_block()) continue;
            if (facet.dynamic()) const_cast<Facet&>(facet).form_bbox();
            if (facet.bbox.maxX < position.x || facet.bbox.minX > position.x) continue;
            if (facet.bbox.maxY < position.y || facet.bbox.minY > position.y) continue;

            point3d<double> intersection{};
            const auto& vertexes = facet.get_vertexes();
            auto facet_pd = facetPlaneDefinition<int>(vertexes);
            if (!planeLineIntersection(position, line_vec, facet_pd, intersection)) continue;
            if (!pointInProjection(intersection, vertexes.size(), vertexes.data())) continue;

            candidates.emplace_back(&facet, intersection.z, facet.is_ceiling());
        }
    }

    if (candidates.empty()) {
        acceptor = {nullptr, 0, true};
        return;
    }

    candidate* closest_above = nullptr;
    candidate* closest_below = nullptr;

    auto target = static_cast<double>(position.z);
    for (auto& c : candidates) {
        if (c.z_intersection > target) {
            if (closest_above == nullptr || c.z_intersection < closest_above->z_intersection) {
                closest_above = &c;
            }
        } else if (closest_below == nullptr || c.z_intersection > closest_below->z_intersection) {
            closest_below = &c;
        }
    }

    bool floor_above = closest_above != nullptr && !closest_above->ceiling;
    bool floor_below = closest_below != nullptr && !closest_below->ceiling;

    if (floor_above && floor_below) {
        if (abs(position.z - closest_above->z_intersection) < abs(position.z - closest_below->z_intersection)) {
            acceptor = {closest_above->facet, static_cast<SpaceSize>(closest_above->z_intersection), false};
        } else {
            acceptor = {closest_below->facet, static_cast<SpaceSize>(closest_below->z_intersection), false};
        }
        return;
    }
    if (floor_below) {
        acceptor = {closest_below->facet, static_cast<SpaceSize>(closest_below->z_intersection), false};
        return;
    }
    if (floor_above) {
        acceptor = {closest_above->facet, static_cast<SpaceSize>(closest_above->z_intersection), true};
        return;
    }

    acceptor = {nullptr, 0, true};
}

