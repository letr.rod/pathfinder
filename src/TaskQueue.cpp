#include "TaskQueue.h"

//// TaskQueue class

int TaskQueue::add_task(Task* task) {
    auto free_spot = [this]{
        for (int i = 0; i < tasks.size(); ++i) {
            if (tasks[i] == nullptr) return i;
        }
        return -1;
    };

    int task_id = free_spot();
    if (task_id < 0) return task_id;

    tasks_access.lock();
    tasks[task_id] = task;
    task->status = Task::queued;
    queue.push(task_id);
    tasks_access.unlock();

    if (threads_active < max_threads) {
        ++threads_active;
        std::thread(&TaskQueue::handler, this).detach();
    }
    return task_id;
}

Task::Status TaskQueue::task_status(int task_id) const {
    if (task_id < 0 || task_id >= tasks.size() || tasks[task_id] == nullptr) return Task::empty;
    auto ptr = tasks[task_id];
    return ptr->status;
}

unsigned int TaskQueue::task_result(int task_id, void *acceptor) {
    if (task_id < 0 || task_id >= tasks.size() || tasks[task_id] == nullptr) return 0;
    Task* task = tasks[task_id];
    if (task->status >= Task::completed) {
        unsigned int result = task->get_result(acceptor);
        delete task;
        tasks[task_id] = nullptr;
        return result;
    }
    return 0;
}

Task *TaskQueue::pop_next_task() {
    if (queue.empty()) return nullptr;
    int task_id = queue.front();
    queue.pop();
    return tasks[task_id];
}

void TaskQueue::handler() {
    while (!interrupt) {
        tasks_access.lock();
        auto task = pop_next_task();
        tasks_access.unlock();

        if (task == nullptr) break;
        task->status = Task::active;
        task->start();
        // if status was not modified inside task function (i.e. error or interrupted)
        if (task->status == Task::active) task->status = Task::completed;
    }

    --threads_active;
    if (interrupt) wip.notify_one();
}

void TaskQueue::stop() {
    if (threads_active == 0) {
        queue = {};
        return;
    }

    interrupt = true;
    std::unique_lock lock_wip(work_in_progress);
    wip.wait(lock_wip, [this]{return threads_active == 0;});
    interrupt = false;
    queue = {};
    for (int i = 0; i < max_tasks; ++i) {
        if (tasks[i] != nullptr) {
            delete tasks[i];
            tasks[i] = nullptr;
        }
    }
}

int TaskQueue::get_max_threads() const {
    return max_threads;
}

void TaskQueue::set_max_threads(int value) {
    max_threads = value;
    if (threads_active < max_threads && threads_active < tasks.size()) {
        for (int i = threads_active; i < std::min(max_threads, max_tasks); ++i) {
            ++threads_active;
            std::thread(&TaskQueue::handler, this).detach();
        }
    }
}

int TaskQueue::get_max_tasks() const {
    return max_tasks;
}

void TaskQueue::set_max_tasks(int value) {
    stop();
    max_tasks = value;
    tasks.resize(max_tasks);
}

int TaskQueue::size() const {
    return queue.size();
}

TaskQueue::TaskQueue() {
    max_threads = 2;
    max_tasks = 50;
    threads_active = 0;
    interrupt = false;
    tasks.resize(max_tasks);
}

TaskQueue::~TaskQueue() {
    stop();
}







