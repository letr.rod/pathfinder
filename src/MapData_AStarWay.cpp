#include "MapData.h"

struct Cell {
    Vertex position;
    double cost;
    unsigned int from;
    unsigned int id;

    Cell(const Vertex& pos, unsigned int _from, unsigned int _id): position(pos), from(_from), id(_id) {
        cost = 0;
    }

    void calc_cost(const Vertex& to, bool flying) {
        cost = squareDistance<double>(position.x, position.y, position.z, to.x, to.y, to.z);
        if (!flying) {
            double z_diff = to.z - position.z;
            if (z_diff > 0) cost += z_diff * z_diff;
        }
    }

    static Cell pop_cheapest(std::vector<Cell> &reachable) {
        auto cheapest = reachable.begin();
        auto it = cheapest;
        ++it;
        for (; it != reachable.end(); ++it) {
            if (it->cost < cheapest->cost) cheapest = it;
        }
        Cell result = *cheapest;
        reachable.erase(cheapest);
        return result;
    }
};

bool MapData::traceBodyLines(const Vertex &from, const Vertex &to, const Object &object, const FacetGroups &groups, vec3d<double> &o_cosine) {
    if (object.radius <= 1) {
        return traceLine(groups, from, to);
    }

    Vertex from_shift {};
    Vertex to_shift {};

    DEBUG_MSG("Shifts:" << std::endl)

    // 90 deg
    auto tmp = o_cosine.x;
    o_cosine.x = -o_cosine.y;
    o_cosine.y = tmp;
    o_cosine.z = 0;

    stepFromPoint(from_shift, from, object.radius, o_cosine);
    stepFromPoint(to_shift, to, object.radius, o_cosine);
    if (!object.flying) {
        from_shift.z = static_cast<SpaceSize>(from_shift.z + object.step_height);
        to_shift.z = static_cast<SpaceSize>(to_shift.z + object.step_height);
    }
    DEBUG_MSG(from_shift.x << ", " << from_shift.y << ", " << from_shift.z);
    DEBUG_MSG(" - " << to_shift.x << ", " << to_shift.y << ", " << to_shift.z << std::endl);
    if (!traceLine(groups, from_shift, to_shift)) return false;

    // -90 deg
    o_cosine.x = -o_cosine.x;
    o_cosine.y = -o_cosine.y;

    stepFromPoint(from_shift, from, object.radius, o_cosine);
    stepFromPoint(to_shift, to, object.radius, o_cosine);
    if (!object.flying) {
        from_shift.z = static_cast<SpaceSize>(from_shift.z + object.step_height);
        to_shift.z = static_cast<SpaceSize>(to_shift.z + object.step_height);
    }
    DEBUG_MSG(from_shift.x << ", " << from_shift.y << ", " << from_shift.z);
    DEBUG_MSG(" - " << to_shift.x << ", " << to_shift.y << ", " << to_shift.z << std::endl);
    if (!traceLine(groups, from_shift, to_shift)) return false;

    return true;
}

bool MapData::traceWay(const Vertex &from, const Vertex &to, const Object &object) const {
    const auto& groups = facet_groups_on_line(from, to, !object.flying);
    auto or_cos = vec3d(from, to).convert<double>().ortho_cosine();

    DEBUG_MSG("Way tracing from/to:" << std::endl)
    DEBUG_MSG(from.x << ", " << from.y << ", " << from.z);
    DEBUG_MSG(" - " << to.x << ", " << to.y << ", " << to.z << std::endl);

    if (!object.flying) {
        FloorInfo this_floor{};

        SpaceSize prev_z = from.z;
        Vertex step;
        auto length = getDistance(from, to);

        SpaceSize step_length = object.step_length;
        SpaceSize position = step_length;
        bool min_step_made = false;
        int min_steps_count = object.step_length / min_step_length;
        while (true) {
            stepFromPoint(step, from, static_cast<SpaceSize>(position), or_cos);
            step.z += object.step_height;
            getFloorLevel(groups, step, this_floor);
            step.z = this_floor.level;
            if (this_floor.in_void) return false;
            if (abs(this_floor.level - prev_z) > object.step_height) {
                if (min_step_made) return false;
                position = position - step_length + min_step_length;
                step_length = min_step_length;
                min_steps_count = object.step_length / min_step_length;
                min_step_made = true;
                continue;
            }
            if (min_step_made) {
                if (min_steps_count > 0) {
                    --min_steps_count;
                } else {
                    min_step_made = false;
                    step_length = object.step_length;
                }
            }
            if (position == length) break;
            position = std::min(static_cast<SpaceSize>(position + step_length), length);
            prev_z = this_floor.level;
        }
    }

    return traceBodyLines(from, to, object, groups, or_cos);
}

FloorInfo MapData::floorLevel(Vertex position) const {
    int x = position.x, y = position.y, z = position.z;
    sector_from_point(x, y, z);
    z = bounding_box.maxZ;

    std::vector<const std::vector<Facet>*> groups;
    for (; z >= bounding_box.minZ; --z) {
        auto group = facet_group_by_sector(x, y, z);
        if (group != nullptr) groups.push_back(group);
    }

    FloorInfo acceptor;
    getFloorLevel(groups, position, acceptor);
    return acceptor;
}

bool MapData::AStarWay(
        Vertex from,
        Vertex to,
        const Object &object,
        unsigned int limit,
        std::vector<Vertex>& acceptor) const {

    std::vector<Vertex> shifts;
    if (object.flying) {
        shifts = {
                {-1,0,0},{-1,1,0},{0,1,0},{1,1,0},{1,0,0},{1,-1,0},{0,-1,0},{-1,-1,0},
                {-1,0,1},{-1,1,1},{0,1,1},{1,1,1},{1,0,1},{1,-1,1},{0,-1,1},{-1,-1,1},
                {-1,0,-1},{-1,1,-1},{0,1,-1},{1,1,-1},{1,0,-1},{1,-1,-1},{0,-1,-1},{-1,-1,-1}
        };
    } else {
        shifts = {
                {-1,0,0},{-1,1,0},{0,1,0},{1,1,0},{1,0,0},{1,-1,0},{0,-1,0},{-1,-1,0}
        };
    }

    SpaceSize step_shift = std::max(object.step_length, min_step_length);
    to.z = static_cast<SpaceSize>(to.z + object.step_height);

    for (auto &shift : shifts) {
        shift.x = static_cast<SpaceSize>(step_shift * shift.x);
        shift.y = static_cast<SpaceSize>(step_shift * shift.y);
        shift.z = static_cast<SpaceSize>(step_shift * shift.z);
    }

    std::map<SpaceSize,std::map<SpaceSize,std::map<SpaceSize,bool>>> explored;
    std::vector<Cell> cells;
    if (limit > 0) cells.reserve(limit + 10);

    std::vector<Cell> reachable;
    if (object.flying) {
        reachable.emplace_back(from, 0, 0);
    } else {
        const FloorInfo &floor_info = floorLevel(from);
        Cell start({from.x, from.y, floor_info.level}, 0, 0);
        reachable.push_back(start);
    }

    DEBUG_MSG("Building A* way:");
    DEBUG_MSG("from: (" << from.x << "," << from.y << "," << from.z << ")," << std::endl);
    DEBUG_MSG("to: (" << to.x << "," << to.y << "," << to.z << ")," << std::endl);
    double threshold = step_shift * 4;
    unsigned int target_cell_id = 0;
    while (!reachable.empty()) {
        const Cell& cell = Cell::pop_cheapest(reachable);
        unsigned int from_id = cells.size();
        cells.push_back(cell);
        cells.back().id = from_id;

        if (getDistance(cell.position, to) < threshold) {
            Vertex check = cell.position;
            if (!object.flying) check.z = static_cast<SpaceSize>(check.z + object.step_height);
            auto groups = facet_groups_on_line(check, to);
            if (traceLine(groups, check, to)) {
                target_cell_id = from_id;
                break;
            }
        }

        if (limit && cells.size() >= limit) break;
        explored[cell.position.x][cell.position.y][cell.position.z] = true;

        for (auto &shift : shifts) {
            Vertex next_pos(cell.position.x + shift.x, cell.position.y + shift.y, cell.position.z + shift.z);

            auto& z_explored = explored[next_pos.x][next_pos.y];
            if (z_explored[next_pos.z]) continue;
            z_explored[next_pos.z] = true;

            if (!object.flying) next_pos.z = static_cast<SpaceSize>(next_pos.z + object.step_height);
            const FloorInfo &floor_info = floorLevel(next_pos);

            if (!object.flying) {
                if (floor_info.floor == nullptr) continue;
                next_pos.z = floor_info.level;
                z_explored[next_pos.z] = true;
            } else if (floor_info.in_void) {
                continue;
            }

            if (!traceWay(cell.position, next_pos, object)) continue;

            reachable.emplace_back(next_pos, from_id, 0);
            reachable.back().calc_cost(to, object.flying);
        }
    }

    DEBUG_MSG("A* way search finished, cells analyzed: " << cells.size() << std::endl);

    if (target_cell_id == 0) {
        DEBUG_MSG("Could not find way." << std::endl);
        return false;
    }

    acceptor.clear();
    Cell *cell = &cells[target_cell_id];
    while (cell->from > 0) {
        acceptor.push_back(cell->position);
        cell = &cells[cell->from];
    }

    DEBUG_MSG("Way length is " << acceptor.size() << " steps." << std::endl);
    return true;

}

bool MapData::AStarWayObjDebug(
        Vertex from,
        Vertex to,
        const Object &object,
        unsigned int limit,
        std::vector<Vertex>& acceptor) const {

    std::vector<Vertex> shifts;
    if (object.flying) {
        shifts = {
                {-1,0,0},{-1,1,0},{0,1,0},{1,1,0},{1,0,0},{1,-1,0},{0,-1,0},{-1,-1,0},
                {-1,0,1},{-1,1,1},{0,1,1},{1,1,1},{1,0,1},{1,-1,1},{0,-1,1},{-1,-1,1},
                {-1,0,-1},{-1,1,-1},{0,1,-1},{1,1,-1},{1,0,-1},{1,-1,-1},{0,-1,-1},{-1,-1,-1}
        };
    } else {
        shifts = {
                {-1,0,0},{-1,1,0},{0,1,0},{1,1,0},{1,0,0},{1,-1,0},{0,-1,0},{-1,-1,0}
        };
    }

    SpaceSize step_shift = std::max(object.step_length, min_step_length);
    to.z = static_cast<SpaceSize>(to.z + object.step_height);

    for (auto &shift : shifts) {
        shift.x = static_cast<SpaceSize>(step_shift * shift.x);
        shift.y = static_cast<SpaceSize>(step_shift * shift.y);
        shift.z = static_cast<SpaceSize>(step_shift * shift.z);
    }

    std::map<SpaceSize,std::map<SpaceSize,std::map<SpaceSize,bool>>> explored;
    std::vector<Cell> cells;
    if (limit > 0) cells.reserve(limit + 10);

    std::vector<Cell> reachable;
    if (object.flying) {
        reachable.emplace_back(from, 0, 0);
    } else {
        const FloorInfo &floor_info = floorLevel(from);
        Cell start({from.x, from.y, floor_info.level}, 0, 0);
        reachable.push_back(start);
    }

    DEBUG_MSG("Building A* way:");
    DEBUG_MSG("from: (" << from.x << "," << from.y << "," << from.z << ")," << std::endl);
    DEBUG_MSG("to: (" << to.x << "," << to.y << "," << to.z << ")," << std::endl);
    double threshold = step_shift * 4;
    unsigned int target_cell_id = 0;
    while (!reachable.empty()) {
        const Cell& cell = Cell::pop_cheapest(reachable);
        unsigned int from_id = cells.size();
        cells.push_back(cell);
        cells.back().id = from_id;

        if (getDistance(cell.position, to) < threshold) {
            Vertex check = cell.position;
            if (!object.flying) check.z = static_cast<SpaceSize>(check.z + object.step_height);
            auto groups = facet_groups_on_line(check, to);
            if (traceLine(groups, check, to)) {
                target_cell_id = from_id;
                break;
            }
        }

        if (limit && cells.size() >= limit) break;
        explored[cell.position.x][cell.position.y][cell.position.z] = true;

        for (auto &shift : shifts) {
            Vertex next_pos(cell.position.x + shift.x, cell.position.y + shift.y, cell.position.z + shift.z);

            auto& z_explored = explored[next_pos.x][next_pos.y];
            if (z_explored[next_pos.z]) continue;
            z_explored[next_pos.z] = true;

            if (!object.flying) next_pos.z = static_cast<SpaceSize>(next_pos.z + object.step_height);
            const FloorInfo &floor_info = floorLevel(next_pos);

            if (!object.flying) {
                if (floor_info.floor == nullptr) continue;
                next_pos.z = floor_info.level;
                z_explored[next_pos.z] = true;
            } else if (floor_info.in_void) {
                continue;
            }

            if (!traceWay(cell.position, next_pos, object)) continue;

            reachable.emplace_back(next_pos, from_id, 0);
            reachable.back().calc_cost(to, object.flying);
        }
    }

    DEBUG_MSG("A* way search finished, cells analyzed: " << cells.size() << std::endl);

    acceptor.clear();
    Cell *cell = &cells[target_cell_id];
    while (cell->from > 0) {
        acceptor.push_back(cell->position);
        cell = &cells[cell->from];
    }

    std::ofstream obj("trace_log.obj");
    if (obj.is_open()) {
        obj << "o Model" << std::endl;

        int current_vertex = 0;
        std::stringstream vertexes, polygons, lines;
        for (auto& facet : facets) {
            if (facet.no_block()) continue;

            polygons << 'f';
            for (auto& vertex : facet.get_vertexes()) {
                vertexes << "v " << vertex.x << ' ' << vertex.y << ' ' << vertex.z << std::endl;
                ++current_vertex;
                polygons << ' ' << current_vertex;
            }
            polygons << std::endl;
        }
    
        lines << "l ";
        for (auto& cell : cells) {
            vertexes << "v " << cell.position.x << " " << cell.position.y << " " << cell.position.z << std::endl;
            ++current_vertex;
            lines << ' ' << current_vertex;
        }
        lines << std::endl;

        lines << "l ";
        for (auto& cell : acceptor) {
            vertexes << "v " << cell.x << " " << cell.y << " " << cell.z + 100 << std::endl;
            ++current_vertex;
            lines << ' ' << current_vertex;
        }
        lines << std::endl;

        obj << vertexes.str();
        obj << polygons.str();
        obj << lines.str();
        obj.close();
    }

    DEBUG_MSG("Way length is " << acceptor.size() << " steps." << std::endl);
    return target_cell_id != 0;

}
