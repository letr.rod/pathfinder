#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>
#include <iostream>

#include "MapData.h"

std::string maps_obj_dir = "../tests/example_maps/";

TEST_CASE("traceWay_general_1") {
    MapData map;
    map.import_obj(maps_obj_dir + "7d24.blv.obj");

    Object obj {false, 33, 66, 40};

    Vertex v1 = {382,-1016,35};
    Vertex v2 = {2761,-1047,35};

    REQUIRE(map.traceWay(v1, v2, obj));
    REQUIRE(map.traceWay(v2, v1, obj));
}

TEST_CASE("traceWay_general_2") {
    MapData map;
    map.import_obj(maps_obj_dir + "7d24.blv.obj");

    Object obj {false, 33, 66, 40};

    Vertex v1 = {-31,42,40};
    Vertex v2 = {-31,-24,40};

    REQUIRE(map.traceWay(v1, v2, obj));
    REQUIRE(map.traceWay(v2, v1, obj));
}

TEST_CASE("traceWay_general_3") {
    MapData map;
    map.cube_area_side = 1024;
    map.import_obj(maps_obj_dir + "oute3.odm.obj");

    Object obj {false, 56, 56, 40};

    Vertex v1 = {-10643, -10513, 160};
    Vertex v2 = {-12155, -10569, 160};

    REQUIRE(map.traceWay(v1, v2, obj));
    REQUIRE(map.traceWay(v2, v1, obj));
}

TEST_CASE("traceWay_general_4") {
    MapData map;
    map.cube_area_side = 1024;
    map.import_obj(maps_obj_dir + "oute3.odm.obj");

    Object obj {false, 56, 56, 40};

    Vertex v1 = {-9886, -10471, 160};
    Vertex v2 = {-10839, -10538, 160};

    REQUIRE(map.traceWay(v1, v2, obj));
    REQUIRE(map.traceWay(v2, v1, obj));
}

TEST_CASE("traceWay_slope_1") {
    MapData map;
    map.import_obj(maps_obj_dir + "d25.blv.obj");

    Object obj {false, 60, 120, 40};

    Vertex v1 = {-85,2120,-32};
    Vertex v2 = {-565,2120,-512};

    REQUIRE(map.traceWay(v1, v2, obj));
    REQUIRE(map.traceWay(v2, v1, obj));
}

TEST_CASE("traceWay_slope_2") {
    MapData map;
    map.import_obj(maps_obj_dir + "d25.blv.obj");

    Object obj {false, 60, 120, 40};

    Vertex v1 = {-895,631,-1170};
    Vertex v2 = {-895,1964,-540};

    REQUIRE(map.traceWay(v1, v2, obj));
    REQUIRE(map.traceWay(v2, v1, obj));
}

TEST_CASE("AStarWay_general_1") {
    MapData map;
    map.import_obj(maps_obj_dir + "7d24.blv.obj");

    std::vector<Vertex> way = {{-4124,-3570,-63},{-4058,-3570,-64},{-3992,-3636,-64},{-3926,-3702,-64},{-3860,-3702,-64},{-3794,-3702,-64},{-3728,-3702,-64},{-3662,-3702,-64},{-3596,-3702,-64},{-3530,-3702,-64},{-3464,-3702,-64},{-3398,-3702,-64},{-3332,-3702,-64},{-3266,-3636,-64},{-3200,-3570,-64},{-3134,-3504,-64},{-3068,-3438,-64},{-3002,-3372,-64},{-2936,-3306,-64},{-2936,-3240,-64},{-2936,-3174,-64},{-2936,-3108,-64},{-2936,-3042,-64},{-2936,-2976,-64},{-2936,-2910,-64},{-2936,-2844,-64},{-2936,-2778,-64},{-2936,-2712,-64},{-2870,-2646,-64},{-2804,-2580,-64},{-2738,-2514,-64},{-2672,-2448,-64},{-2606,-2382,-64},{-2606,-2316,-60},{-2606,-2250,-54},{-2606,-2184,-49},{-2606,-2118,-43},{-2606,-2052,-38},{-2606,-1986,-32},{-2606,-1920,-27},{-2606,-1854,-21},{-2606,-1788,-16},{-2606,-1722,-10},{-2606,-1656,-5},{-2606,-1590,0},{-2606,-1524,0},{-2606,-1458,0},{-2606,-1392,0},{-2606,-1326,0},{-2606,-1260,0},{-2540,-1194,0},{-2474,-1194,0},{-2408,-1194,0},{-2342,-1194,0},{-2276,-1194,0},{-2210,-1194,0},{-2144,-1194,0},{-2078,-1194,0},{-2012,-1194,0},{-1946,-1194,0},{-1880,-1194,0},{-1814,-1194,0},{-1748,-1194,0},{-1682,-1194,0},{-1616,-1194,0},{-1550,-1194,0},{-1484,-1194,0},{-1418,-1194,0},{-1352,-1194,0},{-1286,-1194,0},{-1220,-1194,0},{-1154,-1194,0},{-1088,-1194,0},{-1022,-1194,0},{-956,-1194,0},{-890,-1194,0},{-824,-1194,0},{-758,-1128,0},{-692,-1128,0},{-626,-1128,0}};

    Object obj {false, 33, 66, 40};
    std::vector<Vertex> result;

    REQUIRE(map.AStarWay(way.front(), way.back(), obj, 4000, result));
    REQUIRE(map.AStarWay(way.back(), way.front(), obj, 4000, result));

    std::cout << way.size() << " / " << result.size() << std::endl;
}

TEST_CASE("AStarWay_general_2") {
    MapData map;
    map.import_obj(maps_obj_dir + "7d24.blv.obj");

    std::vector<Vertex> way = {{-1398,-3643,1},{-1983,3114,1}};

    Object obj {false, 33, 66, 40};
    std::vector<Vertex> result;

    REQUIRE(map.AStarWay(way.front(), way.back(), obj, 4000, result));
    REQUIRE(map.AStarWay(way.back(), way.front(), obj, 4000, result));

    std::cout << way.size() << " / " << result.size() << std::endl;
}

TEST_CASE("AStarWay_general_3") {
    MapData map;
    map.import_obj(maps_obj_dir + "7d24.blv.obj");

    std::vector<Vertex> way = {{-4124,-3570,-63},{-1983,3114,1}};

    Object obj {false, 33, 66, 40};
    std::vector<Vertex> result;

    REQUIRE(map.AStarWay(way.front(), way.back(), obj, 4000, result));
    REQUIRE(map.AStarWay(way.back(), way.front(), obj, 4000, result));

    std::cout << way.size() << " / " << result.size() << std::endl;
}

TEST_CASE("AStarWay_general_4") {
    MapData map;
    map.import_obj(maps_obj_dir + "7d24.blv.obj");

    std::vector<Vertex> way = {{-1615,2682,1},{-1423,-3500,-63}};

    Object obj {false, 33, 66, 40};
    std::vector<Vertex> result;

    REQUIRE(map.AStarWay(way.front(), way.back(), obj, 8000, result));
    REQUIRE(map.AStarWay(way.back(), way.front(), obj, 8000, result));

    std::cout << way.size() << " / " << result.size() << std::endl;
}

TEST_CASE("AStarWay_general_5") {
    MapData map;
    map.import_obj(maps_obj_dir + "d25.blv.obj");

    std::vector<Vertex> way = {{443,2109,1},{-733,-51,-1216}};

    Object obj {false, 60, 120, 40};
    std::vector<Vertex> result;

    REQUIRE(map.AStarWay(way.front(), way.back(), obj, 8000, result));
    REQUIRE(map.AStarWay(way.back(), way.front(), obj, 8000, result));

    std::cout << way.size() << " / " << result.size() << std::endl;
}

TEST_CASE("AStarWay_general_6") {
    MapData map;
    map.import_obj(maps_obj_dir + "d25.blv.obj");

    std::vector<Vertex> way = {{-775,31,-1215},{-2493,504,1}};

    Object obj {false, 60, 120, 40};
    std::vector<Vertex> result;

    REQUIRE(map.AStarWay(way.front(), way.back(), obj, 8000, result));
    REQUIRE(map.AStarWay(way.back(), way.front(), obj, 8000, result));

    std::cout << way.size() << " / " << result.size() << std::endl;
}

TEST_CASE("AStarWay_general_7") {
    MapData map;
    map.import_obj(maps_obj_dir + "6d01.blv.obj");

    std::vector<Vertex> way = {{-2093,7288,-511},{-1156,5807,-511}};

    Object obj {false, 60, 120, 40};
    std::vector<Vertex> result;

    REQUIRE(map.AStarWay(way.back(), way.front(), obj, 8000, result));
    REQUIRE(map.AStarWay(way.front(), way.back(), obj, 8000, result));

    std::cout << way.size() << " / " << result.size() << std::endl;
}

TEST_CASE("AStarWay_performance") {
    int limit = 8000;

    MapData map;
    map.cube_area_side = 400;
    map.import_obj(maps_obj_dir + "7d24.blv.obj");

    std::vector<Vertex> way = {{-4124,-3570,-63},{-1983,3114,1}};

    Object obj {false, 33, 66, 40};
    std::vector<Vertex> result;

    obj.flying = false;
    REQUIRE(map.AStarWay(way.front(), way.back(), obj, limit, result));
    obj.flying = true;
    REQUIRE(map.AStarWay(way.front(), way.back(), obj, limit, result));

    obj.flying = false;
    BENCHMARK("indoor 400 - walking") { map.AStarWay(way.front(), way.back(), obj, limit, result); };

    obj.flying = true;
    BENCHMARK("indoor 400 - flying") { map.AStarWay(way.front(), way.back(), obj, limit, result); };

//    map.cube_area_side = 1024;
//    map.make_facet_groups();
//
//    obj.flying = false;
//    BENCHMARK("indoor 1024 - walking") { map.AStarWay(way.front(), way.back(), obj, limit, result); };
//
//    obj.flying = true;
//    BENCHMARK("indoor 1024 - flying") { map.AStarWay(way.front(), way.back(), obj, limit, result); };

//    map.cube_area_side = 400;
//    map.import_obj("../../tests/example_maps/oute3.odm.obj");
//
//    way = {{-12136, -10233, 160},{-17606, -5455, 0}};
//
//    obj.flying = false;
//    BENCHMARK("outdoor 400 - walking") { map.AStarWay(way.front(), way.back(), obj, limit, result); };
//
//    obj.flying = true;
//    BENCHMARK("outdoor 400 - flying") { map.AStarWay(way.front(), way.back(), obj, limit, result); };

//    map.cube_area_side = 1024;
//    map.make_facet_groups();
//
//    obj.flying = false;
//    BENCHMARK("outdoor 1024 - walking") { map.AStarWay(way.front(), way.back(), obj, 4000, result); };
//
//    obj.flying = true;
//    BENCHMARK("outdoor 1024 - flying") { map.AStarWay(way.front(), way.back(), obj, 4000, result); };
}
