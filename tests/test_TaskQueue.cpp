#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

#include "TaskQueue.h"

class TestTask: public Task {
    void start() override {
        std::this_thread::sleep_for(std::chrono::seconds(10));
    };
    unsigned int get_result(void *acceptor) override {
        auto result = static_cast<int*>(acceptor);
        *result = 10;
        return 1;
    };
};

TEST_CASE("TaskQueue_basic") {
    TaskQueue queue;
    int id1 = queue.add_task(new TestTask);
    int id2 = queue.add_task(new TestTask);

    int result;
    while (!queue.task_result(id1, &result)) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    REQUIRE(result == 10);

    while (!queue.task_result(id2, &result)) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    REQUIRE(result == 10);
}

TEST_CASE("TaskQueue_stop") {
    TaskQueue queue;
    queue.set_max_threads(1);
    for (int i = 0; i < 10; ++i) queue.add_task(new TestTask);
    std::this_thread::sleep_for(std::chrono::seconds(2));
    queue.stop();
    REQUIRE(queue.size() == 0);
}

TEST_CASE("TaskQueue_stop_empty") {
    TaskQueue queue;
    queue.set_max_threads(1);
    std::this_thread::sleep_for(std::chrono::seconds(2));
    queue.stop();
    REQUIRE(queue.size() == 0);
}