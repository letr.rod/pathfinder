#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

#include <iostream>
#include "GeometryHelper.h"

TEST_CASE("pointInProjection_basic") {
    {
        point3d<int> p{0, 0, 0};
        point3d<int> polygon[4] = {
                {1,  1,  0},
                {1,  -1, 0},
                {-1, -1, 0},
                {-1, 1,  0}
        };

        REQUIRE(pointInProjection(p, 4, polygon));
    }

    {
        point3d<int> p{1, 0, 0};
        point3d<int> polygon[4] = {
                {1,  1,  0},
                {1,  -1, 0},
                {-1, -1, 0},
                {-1, 1,  0}
        };

        REQUIRE(pointInProjection(p, 4, polygon));
    }

    {
        point3d<int> p{2, 0, 0};
        point3d<int> polygon[4] = {
                {1,  1,  0},
                {1,  -1, 0},
                {-1, -1, 0},
                {-1, 1,  0}
        };

        REQUIRE(!pointInProjection(p, 4, polygon));
        BENCHMARK("planeLineIntersection_intersect_BM") {pointInProjection(p, 4, polygon);};
    }
}

TEST_CASE("pointInProjection_border") {
    point3d<int> p{0, 0, 0};
    point3d<int> polygon[4] = {
            {10000,  10000,  0},
            {10000,  -10000, 0},
            {-10000, -10000, 0},
            {-10000, 10000,  0}
    };

    bool success = true;
    for (int i = 9900; i <= 10100; ++i) {
        bool expected = i <= 10000;
        p.x = i;
        p.y = i;
        p.z = 0;

        bool result = pointInProjection(p, 4, polygon);
        if (result != expected) {
            success = false;
            std::cout << "Unexpected result at " << p.x << ", " << p.y << ", " << p.z << std::endl;
        }
    }
    REQUIRE(success);
}

TEST_CASE("pointInProjection_inbound") {
        point3d<int> p{-78, 480, -83};
        point3d<int> polygon[4] = {
                {-243,  1276,  -556},
                {67,  575, 1290},
                {243, -1276, 556},
                {-67, -575,  -1290}
        };

        REQUIRE(pointInProjection(p, 4, polygon));
    }

TEST_CASE("pointInProjection_outbound") {
        point3d<int> p{-220, -370, 1393};
        point3d<int> polygon[4] = {
                {-243,  1276,  -556},
                {67,  575, 1290},
                {243, -1276, 556},
                {-67, -575,  -1290}
        };

        REQUIRE(!pointInProjection(p, 4, polygon));
    }

TEST_CASE("pointInProjection_random_points") {
    point3d<int> polygon[4] = {
            {10000,  10000,  0},
            {10000,  -10000, 0},
            {-10000, -10000, 0},
            {-10000, 10000,  0}
    };

    auto test_points = new point3d<int>[1000];

    for (int i = 0; i < 1000; ++i) {
        auto &p = test_points[i];
        p.x = rand() % 40000 - 20000;
        p.y = rand() % 40000 - 20000;
        p.z = 0;
    }

    BENCHMARK("pointInProjection_performance") {
        for (int i = 0; i < 1000; ++i) {
            auto &p = test_points[i];
            bool expected = p.x >= -10000 && p.x <= 10000 && p.y >= -10000 && p.y <= 10000;
            bool achieved = pointInProjection(p, 4, polygon);
            if (expected != achieved) {
                std::cout << "Point: " << p.x << ", " << p.y << ", " << p.z
                          << " shows unexpected result. Expected: " << expected << std::endl;
            }
            REQUIRE(achieved == expected);
        }
    };

    delete[] test_points;
}

TEST_CASE("pointInProjection_precision") {
    point3d<short> point {};
    point3d<short> polygon[] = {
            {-3936, -3456, -64},
            {-3936, -3456, 192},
            {-3936, -3648, 192},
            {-3936, -3648, -64}
    };

    point = {-3936, -3692, -29};
    std::cout << pointInProjection(point, 4, polygon) << std::endl;

    point = {-3936, -3670, -29};
    std::cout << pointInProjection(point, 4, polygon) << std::endl;

    point = {-3936, -3652, -29};
    std::cout << pointInProjection(point, 4, polygon) << std::endl;

    point = {-3936, -3643, -29};
    std::cout << pointInProjection(point, 4, polygon) << std::endl;
}

TEST_CASE("planeLineIntersection_no_intersect") {
    point3d<int> plane_v1 {10000, 10000, 0};
    point3d<int> plane_v2 {10000, -10000, 0};
    point3d<int> plane_v3 {-10000, -10000, 0};

    auto pd = getPlaneDefinition<int>(plane_v1, plane_v2, plane_v3);
    std::cout << "pd: A = " << pd.A << ", B = " << pd.B << ", C = " << pd.C << ", D = " << pd.D << std::endl;

    point3d<int> line_point {0, 0, 1000};
    vec3d<int> line_vec {0, 1, 0};
    point3d<int> acceptor {};

    for (int i = 0; i < 1000; ++i) {
        line_point.x = rand() % 40000 - 20000;
        line_point.y = rand() % 40000 - 20000;
        line_vec.x = rand() % 2;
        line_vec.y = rand() % 2;
        REQUIRE((line_vec.zero() || !planeLineIntersection(line_point, line_vec, pd, acceptor)));
    }
}

TEST_CASE("planeLineIntersection_intersect") {
    point3d<int> plane_v1 {10000, 10000, 0};
    point3d<int> plane_v2 {10000, -10000, 0};
    point3d<int> plane_v3 {-10000, -10000, 0};

    auto pd = getPlaneDefinition<int>(plane_v1, plane_v2, plane_v3);
    std::cout << "pd: A = " << pd.A << ", B = " << pd.B << ", C = " << pd.C << ", D = " << pd.D << std::endl;

    point3d<int> line_point {0, 0, 1000};
    point3d<int> line_point2 {0, 0, -1000};
    point3d<int> acceptor {};

    for (int i = 0; i < 1000; ++i) {
        line_point2.x = rand() % 19999 - 9999;
        line_point2.y = rand() % 19999 - 9999;
        auto line_vec = vec3d<int>(line_point, line_point2);
        REQUIRE(planeLineIntersection(line_point, line_vec, pd, acceptor));
        std::cout << "Intersection at: " << acceptor.x << ", " << acceptor.y << ", " << acceptor.z << std::endl;
    }

    auto line_vec = vec3d<int>(line_point, line_point2);
    BENCHMARK("planeLineIntersection_intersect_BM") {planeLineIntersection(line_point, line_vec, pd, acceptor);};
}

TEST_CASE("VectorMath_misc") {
    point3d<int> start {0,0,0};
    vec3d<int> vec {1,2,3};
    int length = 2;

    auto oc = vec.ortho_cosine();
    std::cout << "cx: " << oc.x << ", cy: " << oc.y << ", cz: " << oc.z << std::endl;

    double x = start.x + length * oc.x;
    double y = start.y + length * oc.y;
    double z = start.z + length * oc.z;

    std::cout << "x: " << x << ", y: " << y << ", z: " << z << std::endl;
    std::cout << (sqrt(x*x + y*y + z*z)) << std::endl;
}
