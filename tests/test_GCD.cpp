#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

#include <iostream>
#include <vector>

#include "../include/MathHelper.h"

TEST_CASE("GCD") {
    std::vector<std::vector<int>> cases = {
            {2, 2, 2},
            {3, 2, 2},
            {3, 0, 2},
            {1, 1, 1},
            {51, 0, 0},
            {9, 6, 3},
            {7, 7, 49},
    };

    std::vector<int> expected = {
            2, 1, 1, 1, 51, 3, 7
    };

    for (int i = 0; i < cases.size(); ++i) {
        auto& numbers = cases[i];
        REQUIRE(getGCD(numbers.size(), numbers.data()) == expected[i]);
    }

    BENCHMARK("GCD performance") {
        for (auto & numbers : cases) getGCD(numbers.size(), numbers.data());
    };

    BENCHMARK("GCD manual preserve input performance") {
        for (auto & numbers : cases) {
            int data[3] = {numbers[0], numbers[1], numbers[2]};
            getGCD(numbers.size(), data);
        }
    };
}