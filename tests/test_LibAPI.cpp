#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

#include "library.h"

TEST_CASE("Async") {
    auto instance = create_instance();

    MapData& map = *map_data(instance);
    map.import_obj("../../tests/example_maps/7d24.blv.obj");

    int task_id = add_task(instance, false, 33, 33, 40, -1423, -3500, -63, -1615, 2682, 1, 8000);
    while (task_status(instance, task_id) < 3);

    auto result = new Vertex[8000];
    task_result(instance, task_id, result);
    delete[] result;
    delete_instance(instance);
}
