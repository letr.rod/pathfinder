#include "MapData.h"

void MapData::add_outdoor_models(int total_models, const uint8_t* models) {
    auto add_facet = [this](const uint8_t* ptr, const uint16_t *vertex_ids, const point3d<int> *all_vertexes) {
        facets.push_back({});
        Facet &facet = facets.back();

        facet.bits = (unsigned int *)(ptr + 0x1c);
        facet.bounding_box = (short *)(ptr + 0x116);
        facet.polygon_type = ptr + 0x12f;

        uint8_t vertex_count = *(ptr + 0x12e);

        DEBUG_MSG("Facet #" << facet_id << ", vertex count: " << (int)vertex_count << std::endl);
        facet.source_vertexes.resize(vertex_count);
        for (int i = 0; i < vertex_count; ++i) {
            auto id = vertex_ids[i];
            auto vertex_ptr = all_vertexes + id;
            auto &vptr = facet.source_vertexes[i];
            vptr.x = (SpaceSize*)&(vertex_ptr->x);
            vptr.y = (SpaceSize*)&(vertex_ptr->y);
            vptr.z = (SpaceSize*)&(vertex_ptr->z);
        }
        facet.update_vertexes();
        facet.form_bbox();
    };

    for (int model_id = 0; model_id < total_models; ++model_id) {
        auto model_ptr = models + model_id * 0xBC;
        auto vertexes = *(point3d<int>**)(model_ptr + 0x48);
        auto vertex_count = *(int*)(model_ptr + 0x44);

        auto facets_count = *(int*)(model_ptr + 0x4c);
        auto model_facets = *(uint8_t**)(model_ptr + 0x54);
        for (int facet_id = 0; facet_id < facets_count; ++facet_id) {
            auto facet_ptr = model_facets + 0x134 * facet_id;
            auto vertex_ids = (uint16_t*)(facet_ptr + 0x20);
            uint8_t facet_vertex_count = *(facet_ptr + 0x12e);
            add_facet(facet_ptr, vertex_ids, vertexes);
        }
    }
}

void MapData::add_outdoor_ground(int tris_count, const short* tris_vertex_ids, const Vertex* all_vertexes) {
    for (int i = 0; i < tris_count; ++i) {
        facets.push_back({});
        Facet &facet = facets.back();
        for (int j = 0; j < 3; ++j) {
            facet.vertexes.push_back(all_vertexes[tris_vertex_ids[i * 3 + j]]);
        }
        facet.polygon_type = &Facet::default_ground_type;
        facet.bits = &Facet::default_ground_bits;
        facet.form_bbox();
    }
}

void MapData::add_outdoor_cube(int radius, const Vertex* point) {
    Vertex vertexes[8] = {
            {point->x + radius, point->y + radius, point->z + radius},
            {point->x + radius, point->y + radius, point->z - radius},

            {point->x - radius, point->y + radius, point->z + radius},
            {point->x - radius, point->y + radius, point->z - radius},

            {point->x + radius, point->y - radius, point->z + radius},
            {point->x + radius, point->y - radius, point->z - radius},

            {point->x - radius, point->y - radius, point->z + radius},
            {point->x - radius, point->y - radius, point->z - radius},
    };

    auto make_facet = [this, &vertexes](int v1, int v2, int v3, int v4){
        facets.push_back({});
        Facet &facet = facets.back();
        facet.vertexes.push_back(vertexes[v1]);
        facet.vertexes.push_back(vertexes[v2]);
        facet.vertexes.push_back(vertexes[v3]);
        facet.vertexes.push_back(vertexes[v4]);
        facet.polygon_type = &Facet::default_cube_type;
        facet.bits = &Facet::default_cube_bits;
        facet.form_bbox();
    };

    make_facet(0,1,3,2);
    make_facet(0,1,5,4);
    make_facet(4,5,7,6);
    make_facet(6,7,3,2);
}

void MapData::init_indoor(int total_vertexes, int facets_count, const uint8_t* mm_facets, Vertex* vertexes) {

    auto add_facet = [mm_facets, vertexes, total_vertexes](Facet& facet, unsigned short facet_id) {
        auto ptr = mm_facets + facet_id * 96;

        facet.bits = (unsigned int *)(ptr + 0x2c);
        facet.bounding_box = (short *)(ptr + 0x50);
        facet.polygon_type = ptr + 0x5c;

        uint8_t vertex_count = *(ptr + 0x5d);
        auto vertex_ids = (short *)(*(unsigned int *)(ptr + 0x30));

        DEBUG_MSG("Facet #" << facet_id << ", vertex count: " << (int)vertex_count << std::endl);
        facet.source_vertexes.resize(vertex_count);
        facet.vertexes.resize(vertex_count);
        for (int i = 0; i < vertex_count; ++i) {
            auto id = vertex_ids[i];
            if (id < total_vertexes) {
                auto vertex_ptr = vertexes + id;
                auto &vptr = facet.source_vertexes[i];
                vptr.x = &(vertex_ptr->x);
                vptr.y = &(vertex_ptr->y);
                vptr.z = &(vertex_ptr->z);
            }
        }
        facet.update_vertexes();
        facet.form_bbox();
    };

    facets.resize(facets_count);
    for (int i = 0; i < facets_count; ++i) {
        add_facet(facets[i], i);
    }
    make_facet_groups();
}
