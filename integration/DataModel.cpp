#include "DataModel.h"

//// VertexPtr struct

Vertex VertexPtr::get() const {
    return {*x, *y, *z};
}

//// Facet class

bool Facet::no_block() const {
    return (*bits & 0x2000) // invisible
        && (*bits & 0x20000000) // untouchable
        || (*bits & 0x1); // portal
}

bool Facet::is_floor() const {
    return *polygon_type == Facet::horizontalFloor || *polygon_type == Facet::irregularFloor;
}

bool Facet::is_ceiling() const {
    return *polygon_type == Facet::horizontalCeiling || *polygon_type == Facet::irregularCeiling;
}

bool Facet::dynamic() const {
    return *bits & 0x40000; // move by door
}

void Facet::form_bbox() {
    if (bounding_box != nullptr) {
        bbox.minX = bounding_box[0];
        bbox.maxX = bounding_box[1];
        bbox.minY = bounding_box[2];
        bbox.maxY = bounding_box[3];
        bbox.minZ = bounding_box[4];
        bbox.maxZ = bounding_box[5];
        return;
    }

    update_vertexes();
    if (vertexes.empty()) return;

    const Vertex& first = vertexes.front();
    bbox.set(first.x, first.y, first.z);

    for (unsigned int i = 1; i < vertexes.size(); ++i) {
        const Vertex& vertex = vertexes[i];
        bbox.adjust(vertex.x, vertex.y, vertex.z);
    }
};

void Facet::update_vertexes() {
    unsigned int vertexes_count = source_vertexes.size();
    if (vertexes.size() < vertexes_count) vertexes.resize(vertexes_count);
    for (unsigned int i = 0; i < vertexes_count; ++i) vertexes[i] = source_vertexes[i].get();
}

const std::vector<Vertex>& Facet::get_vertexes() const {
    if (dynamic()) const_cast<Facet*>(this)->update_vertexes();
    return vertexes;
}



