#pragma once

#include "DebugLog.h"
#include "MapData.h"
#include "TaskQueue.h"

class PathfinderTask: public Task {
public:
    PathfinderTask() = delete;
    PathfinderTask(MapData* _map, Vertex _from, Vertex _to, Object _obj, uint32_t _limit);
    ~PathfinderTask() override = default;
    void start() override;
    unsigned int get_result(void *acceptor) override;

    MapData* map;
    Vertex from;
    Vertex to;
    Object object;
    unsigned int limit;

private:
    std::vector<Vertex> result;
};

struct Pathfinder {
    MapData mapData;
    TaskQueue queue;
};

//// library init

Pathfinder* create_instance();

void delete_instance(Pathfinder* instance);

void stop(Pathfinder* instance);

void dump_model(Pathfinder* instance, const char * filepath);

//// data init

void init_indoor_map(Pathfinder* instance, int total_facets, int total_vertexes, const uint8_t* mm_facets, Vertex* vertexes);

void init_outdoor_map(Pathfinder* instance);

void add_outdoor_models(Pathfinder* instance, int total_models, const uint8_t* models);

void add_outdoor_ground(Pathfinder* instance, int tris_count, const short* tris_vertex_ids, const Vertex* all_vertexes);

void add_outdoor_cube(Pathfinder* instance, int radius, int x, int y, int z);

void make_outdoor_areas(Pathfinder* instance);

//// synchronous interaction

unsigned int AStarWay(Pathfinder* instance, bool flying, int obj_radius, int step_length, int step_height, int x, int y, int z, int to_x, int to_y, int to_z, int limit, Vertex *acceptor);

unsigned int AStarWayObjDebug(Pathfinder* instance, bool flying, int obj_radius, int step_length, int step_height, int x, int y, int z, int to_x, int to_y, int to_z, int limit, Vertex *acceptor);

int get_floor_level(Pathfinder* instance, int x, int y, int z);

bool trace_line(Pathfinder* instance, int x, int y, int z, int to_x, int to_y, int to_z);

bool trace_way(Pathfinder* instance, bool flying, int radius, int step_height, int x, int y, int z, int to_x, int to_y, int to_z);

//// asynchronous interaction

int add_task(Pathfinder* instance, bool flying, int obj_radius, int step_length, int step_height, int x, int y, int z, int to_x, int to_y, int to_z, int limit);

int task_status(Pathfinder* instance, int task_id);

unsigned int task_result(Pathfinder* instance, int task_id, Vertex *acceptor);

//// asynchronous settings

int get_max_threads(Pathfinder* instance);

void set_max_threads(Pathfinder* instance, int value);

int get_max_tasks(Pathfinder* instance);

void set_max_tasks(Pathfinder* instance, int value);

//// testing

MapData* map_data(Pathfinder* instance);
