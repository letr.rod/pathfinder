#include "library.h"

//// PathfinderTask class

void PathfinderTask::start() {
    if (map == nullptr) return;
    map->AStarWay(from, to, object, limit, result);
}

unsigned int PathfinderTask::get_result(void *acceptor) {
    auto ptr = static_cast<Vertex*>(acceptor);
    unsigned int pos = result.size();
    for (auto & cell : result) {
        --pos;
        ptr[pos] = cell;
    }
    return result.size();
}

PathfinderTask::PathfinderTask(MapData *_map, Vertex _from, Vertex _to, Object _obj, uint32_t _limit):
    map(_map), from(_from), to(_to), object(_obj), limit(_limit) {}


//// library init

Pathfinder* create_instance() {
    return new Pathfinder;
}

void delete_instance(Pathfinder* instance) {
    if (instance == nullptr) return;
    delete instance;
}

void stop(Pathfinder* instance) {
    if (instance == nullptr) return;
    instance->queue.stop();
}

void dump_model(Pathfinder* instance, const char * filepath) {
    if (instance == nullptr) return;
    instance->mapData.dump(filepath);
}

//// data init

void init_indoor_map(Pathfinder* instance, int total_facets, int total_vertexes, const uint8_t* mm_facets, Vertex* vertexes) {
    if (instance == nullptr) return;
    instance->queue.stop();
    instance->mapData.init_indoor(total_vertexes, total_facets, mm_facets, vertexes);
}

void init_outdoor_map(Pathfinder* instance) {
    if (instance == nullptr) return;
    instance->queue.stop();
    instance->mapData.facets.clear();
}

void add_outdoor_models(Pathfinder* instance, int total_models, const uint8_t* models) {
    if (instance == nullptr) return;
    instance->queue.stop();
    instance->mapData.add_outdoor_models(total_models, models);
}

void add_outdoor_ground(Pathfinder* instance, int tris_count, const short* tris_vertex_ids, const Vertex* all_vertexes) {
    if (instance == nullptr) return;
    instance->queue.stop();
    instance->mapData.add_outdoor_ground(tris_count, tris_vertex_ids, all_vertexes);
}

void add_outdoor_cube(Pathfinder* instance, int radius, int x, int y, int z) {
    if (instance == nullptr) return;
    instance->queue.stop();
    Vertex v(x, y, z);
    instance->mapData.add_outdoor_cube(radius, &v);
}

void make_outdoor_areas(Pathfinder* instance) {
    if (instance == nullptr) return;
    instance->mapData.make_facet_groups();
}

//// synchronous interaction

unsigned int AStarWay(Pathfinder* instance, bool flying, int obj_radius, int step_length, int step_height,
                      int x, int y, int z,
                      int to_x, int to_y, int to_z,
                      int limit, Vertex *acceptor) {

    if (instance == nullptr) return 0;

    Vertex from(x, y, z);
    Vertex to(to_x, to_y, to_z);
    Object obj(flying, obj_radius, step_length, step_height);

    std::vector<Vertex> result;
    bool success = instance->mapData.AStarWay(from, to, obj, limit, result);
    unsigned int count = result.size();
    if (success) {
        unsigned int pos = result.size();
        for (auto & cell : result) {
            --pos;
            acceptor[pos] = cell;
        }
    }
    return count;
}

unsigned int AStarWayObjDebug(Pathfinder* instance, bool flying, int obj_radius, int step_length, int step_height,
                      int x, int y, int z,
                      int to_x, int to_y, int to_z,
                      int limit, Vertex *acceptor) {

    if (instance == nullptr) return 0;

    Vertex from(x, y, z);
    Vertex to(to_x, to_y, to_z);
    Object obj(flying, obj_radius, step_length, step_height);

    std::vector<Vertex> result;
    bool success = instance->mapData.AStarWayObjDebug(from, to, obj, limit, result);
    unsigned int count = result.size();
    if (success) {
        unsigned int pos = result.size();
        for (auto & cell : result) {
            --pos;
            acceptor[pos] = cell;
        }
    }
    return count;
}

int get_floor_level(Pathfinder* instance, int x, int y, int z) {
    if (instance == nullptr) return -30000;

    Vertex position(x, y, z);
    const FloorInfo& floor_info = instance->mapData.floorLevel(position);
    if (!floor_info.floor) return -30000;
    return floor_info.level;
}

bool trace_line(Pathfinder* instance, int x, int y, int z, int to_x, int to_y, int to_z) {
    if (instance == nullptr) return false;

    Vertex from(x, y, z);
    Vertex to(to_x, to_y, to_z);
    const auto& groups = instance->mapData.facet_groups_on_line(from, to);
    return traceLine(groups, from, to);
}

bool trace_way(Pathfinder* instance, bool flying, int radius, int step_height, int x, int y, int z, int to_x, int to_y, int to_z) {
    if (instance == nullptr) return false;

    Vertex from(x, y, z);
    Vertex to(to_x, to_y, to_z);
    return instance->mapData.traceWay(from, to, {flying, radius, radius, step_height});
}

//// asynchronous interaction

int add_task(Pathfinder* instance, bool flying, int obj_radius, int step_length, int step_height, int x, int y, int z, int to_x, int to_y, int to_z, int limit) {
    if (instance == nullptr) return -1;

    Vertex from(x, y, z);
    Vertex to(to_x, to_y, to_z);
    Object obj(flying, obj_radius, step_length, step_height);

    auto task = new PathfinderTask(&instance->mapData, from, to, obj, limit);
    return instance->queue.add_task(task);
}

int task_status(Pathfinder* instance, int task_id) {
    if (instance == nullptr) return 0;

    return instance->queue.task_status(task_id);
}

unsigned int task_result(Pathfinder* instance, int task_id, Vertex *acceptor) {
    if (instance == nullptr) return 0;
    int count = instance->queue.task_result(task_id, acceptor);
    return count;
}

//// asynchronous settings

int get_max_threads(Pathfinder* instance) {
    if (instance == nullptr) return 0;
    return instance->queue.get_max_threads();
}

void set_max_threads(Pathfinder* instance, int value) {
    if (instance == nullptr) return;
    instance->queue.set_max_threads(value);
}

int get_max_tasks(Pathfinder* instance) {
    if (instance == nullptr) return 0;
    return instance->queue.get_max_tasks();
}

void set_max_tasks(Pathfinder* instance, int value) {
    if (instance == nullptr) return;
    instance->queue.set_max_tasks(value);
}

//// testing

MapData* map_data(Pathfinder* instance) {
    if (instance == nullptr) return nullptr;
    return &instance->mapData;
}