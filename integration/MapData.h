#pragma once

#include "DataModel.h"
#include "Tracing.h"

#include <sstream>
#include <fstream>
#include <map>
#include <set>
#include <ctime>

using FacetGroups = std::vector<const std::vector<Facet>*>;

const SpaceSize default_area_size = 400;
const SpaceSize default_min_step_length = 24;

struct Object {
    bool flying;
    SpaceSize radius;
    SpaceSize step_length;
    SpaceSize step_height;

    Object(bool _flying, int _radius, int _step_length, int _step_height);
};

class MapData {
public:

    //// implementation-defined members

    void add_outdoor_models(int total_models, const uint8_t* models);
    void add_outdoor_ground(int tris_count, const short* tris_vertex_ids, const Vertex* all_vertexes);
    void add_outdoor_cube(int radius, const Vertex* point);
    void init_indoor(int total_vertexes, int total_facets, const uint8_t* mm_facets, Vertex* vertexes);

    //// required internal members

    SpaceSize min_step_length = default_min_step_length;
    SpaceSize cube_area_side = default_area_size;
    BoundingBox bounding_box;

    std::vector<Facet> facets; // all facets of the map

    void make_facet_groups();

    void sector_from_point(ExtSpaceSize &x, ExtSpaceSize &y, ExtSpaceSize &z) const;
    void sector_from_point(SpaceSize &x, SpaceSize &y, SpaceSize &z) const;

    [[nodiscard]] const std::vector<Facet>* facet_group_by_sector(int x, int y, int z) const;
    [[nodiscard]] const std::vector<Facet>* facet_group_from_point(SpaceSize x, SpaceSize y, SpaceSize z) const;

    [[nodiscard]] FacetGroups facet_groups_on_vertical(Vertex from, Vertex to, bool grounded = false) const;
    [[nodiscard]] FacetGroups facet_groups_on_axis(Vertex from, Vertex to, vec3d<SpaceSize> vec, bool grounded) const;
    [[nodiscard]] FacetGroups facet_groups_on_line(Vertex from, Vertex to, bool grounded = false) const;

    [[nodiscard]] static bool traceBodyLines(const Vertex &from, const Vertex &to, const Object &object, const FacetGroups &groups, vec3d<double> &o_cosine);
    [[nodiscard]] bool traceWay(const Vertex &from, const Vertex &to, const Object &object) const;
    [[nodiscard]] FloorInfo floorLevel(Vertex position) const;

    bool AStarWay(Vertex from, Vertex to, const Object &object, unsigned int limit, std::vector<Vertex>& acceptor) const;

    //// testing / lookup

    void import_obj(const std::string& obj_filepath);
    void dump(const char* filepath) const;
    bool AStarWayObjDebug(Vertex from, Vertex to, const Object &object, unsigned int limit, std::vector<Vertex>& acceptor) const;


private:
    std::map<int,std::map<int,std::map<int,std::vector<Facet>>>> facet_groups;
};