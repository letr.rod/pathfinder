#pragma once

#pragma pack(push)
#pragma pack(2)

#include "DebugLog.h"
#include "GeometryHelper.h"
#include "VectorMath.h"

#include <vector>

using SpaceSize = short;
using ExtSpaceSize = int; // used for loop variables to avoid number overflow in operations on coordinates close to SpaceSize capacity.
using Vertex = point3d<SpaceSize>;
using BoundingBox = BBox<SpaceSize>;

#pragma pack(pop)

struct VertexPtr {
    SpaceSize* x;
    SpaceSize* y;
    SpaceSize* z;

    Vertex get() const;
};

struct Facet {
    //// implementation-defined members

    enum PolygonType {
        empty = 0,
        wall = 1,
        unused = 2,
        horizontalFloor = 3,
        irregularFloor = 4,
        horizontalCeiling = 5,
        irregularCeiling = 6,
    };

    static const uint8_t default_ground_type = Facet::irregularFloor;
    static const uint8_t default_cube_type = Facet::wall;
    static const unsigned int default_ground_bits = 0;
    static const unsigned int default_cube_bits = 0;

    const unsigned int *bits = nullptr;
    const int16_t *bounding_box;
    const uint8_t *polygon_type;
    std::vector<Vertex> vertexes;
    std::vector<VertexPtr> source_vertexes;

    void update_vertexes();

    //// generic required members

    BoundingBox bbox;

    bool no_block() const;
    bool is_floor() const;
    bool is_ceiling() const;
    bool dynamic() const;

    void form_bbox();
    const std::vector<Vertex>& get_vertexes() const;
};


