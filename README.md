## 3D Pathfinding engine
Engine initially made for Might and Magic VIII, but I hope, it can be adapted to other projects. Files, I assume to be edited during adaptation, are stored in "Integration" folder.

### Dependencies:
1. Standard library.
2. [Catch2](https://github.com/catchorg/Catch2) as test framework.

### Documentation:
No documentation at the moment :(\
[Example](https://gitlab.com/letr.rod/mmmerge/-/blob/Rodril_nightly_build/Scripts/Modules/PathfinderDll.lua) of integration into MMMerge.


CC0 Free software
